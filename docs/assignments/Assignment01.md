# Assignment 01

### Deadline:

January <s>3<sup>rd</sup></s> 7<sup>th</sup> 2023


### Format:

Work in pairs


### Delivery:

Python script via E-mail. It **must** be named `FirstName_LastName_StudentNumber-FirstName_LastName_StudentNumber-mfinder.py`.


## Introduction

Now that you already know most of Python's data structures, conditionals, loops and file I/O, you are ready to start working on your first real program. This one is about finding DNA *motifs* in sequence data.


## Objectives

Create a program that reads a FASTA file, finds DNA *motifs* in it and reports its findings.


## Rules

* The program must be written in Python 3
* The program must be linear -> Do not write any functions or classes (leave that for the next assignment)
* You can import modules from Python's standard library (those included with Python), but not from third party packages (like BioPython)
* The FASTA file must be provided to the program as a command line argument
* The *motif* must be read from STDIN
    * The *motif* must be at least 5bp long
    * The *motif* must contain only the 4 DNA bases IUPAC codes (A, T, G, or C)
        * Ambiguities can be ignored for this assignment
* If the *motif* is present multiple times in the same sequence, your program must report this
    * *Motifs* must be reported in the order they are found: a *motif* in position 3 must be reported before a motif in position 29
* The sequence length must be reported
* The frame where the motif was found must be reported as well
    * If this is the case, the positive frames in which the motif was found must be reported before negative ones.
* Your program must write the results to STDOUT, similar to the example below:
    * **If your output does not respect this scheme you will automatically fail the assignment**
    * The field separator character must be a TAB (`\t` in Python)

```
Sequence_name	Sequence_length	Motif_position	Frame
```

Or, an example with a real sequence names (but not necessarily real values):

```
MN015201.1 Psammodromus algirus isolate MV.LAC-187 cytochrome b (cytb) gene	1143	18	3
MN015201.1 Psammodromus algirus isolate MV.LAC-187 cytochrome b (cytb) gene	1143	1143	-1
MF684932.1 Psammodromus algirus isolate 23902 cytochrome b (cytb) gene, partial cds; mitochondrial	392	1	1
```


## Hints:

* You can read arguments from the command line (such as the name of a file for reading) using `sys.argv`. Read more about it [here](https://www.geeksforgeeks.org/how-to-use-sys-argv-in-python/).
* The function `sys.exit()` will immediately terminate a program. Learn more about it [here](https://www.geeksforgeeks.org/python-exit-commands-quit-exit-sys-exit-and-os-_exit/).
* You can read from STDIN with the `input()` function. Read more about it [here](https://www.geeksforgeeks.org/python-input-function/).
* Python's `print()` function writes to STDOUT by default.
* [Here](https://www.ncbi.nlm.nih.gov/Class/MLACourse/Modules/MolBioReview/readingframes.html) is a quick read on what DNA frames are, in case you have forgotten.
* Getting the frame right is by far the hardest part of the work. If you are unable to do it, just hard-code the value `F` in it's position. You will get a lower grade, but won't fail the assignment due to a wrong output format.
* Never, *ever*, **ever** submit any code you haven't thoroughly tested with at least 3 or 4 different inputs.
* Beware when taking inspiration from other people's code. Each submission will be automatically checked for plagiarism. Any evidence of such activities will result in a 0 for both submissions. No exceptions.
