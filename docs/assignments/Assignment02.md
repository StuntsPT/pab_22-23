# Assignment 02


### Deadline:

January 31st 2023


### Format:

Work in pairs


### Delivery:

Python script via E-mail. It **must** be named `FirstName_LastName_StudentNumber-FirstName_LastName_StudentNumber-converter.py`.


## Introduction

The learning of lower level python skills is now fading far in the rear view mirror, and you are growing as a coder. An important skill is to know how to use functions and classes, which you absolutely **must** use for this task.

As you already know, bioinformatics uses various file formats for different programs. However, many of them convey the same data, despite being organized in a different way. For this assignment, you will have to write a program that is able to convert files between some of these formats.


## Objectives

Create a program that is able to convert sequence files between three formats: FASTA, Nexus and Phylip. Your program will be provided with an input and an output file. It mus then figure out the input file format, and convert it to another format (based on the output file extension).


## Rules

* The program must be written in Python 3.
* The program cannot be linear -> Most of your code (more than 90%) has to be inside functions and classes.
* All functions **must** take at least one parameter, but not all of them have to return something.
* You can import modules from Python's standard library (those included with Python), but not from third party packages (like BioPython).
* The input file must be provided to the program as a command line argument.
* The output file must be provided to the program as a command line argument.
* The program can use the file extension to infer the **output format**, but not the **input format**.
* The sequences from the input file must be read into an "intermediate representation": an instance of a class called `Sequence`. This class can have as many proprieties as you want, but needs to have *at least* one method for obtaining the sequence's length.
* The Nexus and Phylip formats have two variants - "leave" and "interleave". For this assignment, you only need to worry about the "leave" variant, and can safely ignore that the "interleave" variant even exists (both for input and output).
* When reading or writing a nexus file, you only need to read/write the "data" block. Any other blocks can be safely ignored.
* For this exercise you can also assume that the "DATATYPE" is always DNA, the "MISSING" character is always "N", and the "GAP" character is always "-" (relevant only for Nexus files).
* When writing Nexus or Phylip files, you don't have to worry about aligning sequence names.


## Hints

* Here are example [Nexus](aligned_sequences.nexus) and [Pyhlip](aligned_sequences.phy) files to help you get started. If by now you still don know how to obtain a valid FASTA file you have bigger problems than this assignment.
* [Here](http://hydrodictyon.eeb.uconn.edu/eebedia/index.php/Phylogenetics:_NEXUS_Format) is the documentation on Nexus files
* [Here](http://www.phylo.org/tools/obsolete/phylip.html) is the documentation on Phylip files
* One good way to test your script is to "convert" a file to the same format (Eg. read Nexus and write Nexus) - If the output file is different from the input file, you have gotten it wrong. Make sure you test this for all file types you mean to support.


## Optional "fluff"

* If you want to provide a better user experience, it is recommended that you use an argument parser like [argparse](https://docs.python.org/3/library/argparse.html). If you choose to do this, replace `-converter.py` in your filename with `-converter_ap.py`, and use the switch `-i` for input and the switch `-o` for output.
