### classes.cd[1] = "Introducing Python"

#### Programação Aplicada à Bioinformática 2022-2023

![Logo EST](presentation_assets/logo-ESTB.png)

Francisco Pina Martins

[@FPinaMartins](https://twitter.com/FPinaMartins)

---

### Summary

* &shy;<!-- .element: class="fragment" -->Introducing Python
* &shy;<!-- .element: class="fragment" -->Basic usage
* &shy;<!-- .element: class="fragment" -->Key concepts
* &shy;<!-- .element: class="fragment" -->Data types part 1:
  * &shy;<!-- .element: class="fragment" -->Types summary
  * &shy;<!-- .element: class="fragment" -->Numerical data

---

### Python

* &shy;<!-- .element: class="fragment" -->One of the [most used](https://insights.stackoverflow.com/survey/2021#section-most-popular-technologies-programming-scripting-and-markup-languages) programming languages
* &shy;<!-- .element: class="fragment" -->Also one of the [most loved](https://insights.stackoverflow.com/survey/2021#section-most-loved-dreaded-and-wanted-programming-scripting-and-markup-languages)
* &shy;<!-- .element: class="fragment" -->As [seen previously](https://doi.org/10.1186%2F1471-2105-9-82), one of the most used in bioinformatics as well
* &shy;<!-- .element: class="fragment" -->Has a very large community
* &shy;<!-- .element: class="fragment" -->Has a ridiculous amount of ready to use [third party packages](https://pypi.org/)

&shy;<!-- .element: class="fragment" -->![Python logo](cd01_assets/python-logo.png)

---

### Python characteristics

* &shy;<!-- .element: class="fragment" -->High level
* &shy;<!-- .element: class="fragment" -->General purpose
* &shy;<!-- .element: class="fragment" -->Interpreted
* &shy;<!-- .element: class="fragment" -->Dynamic & strong typed
* &shy;<!-- .element: class="fragment" -->Garbage collected
* &shy;<!-- .element: class="fragment" -->Multi-paradigm
* &shy;<!-- .element: class="fragment" -->"Batteries included"

&shy;<!-- .element: class="fragment" -->![Newbie Friendly](cd01_assets/friendly.png)

---

### Key concepts

* &shy;<!-- .element: class="fragment" -->Data
  * &shy;<!-- .element: class="fragment" -->Simple - text or numbers
  * &shy;<!-- .element: class="fragment" -->Complex - class or container
* &shy;<!-- .element: class="fragment" -->[Keywords](https://docs.python.org/3/reference/lexical_analysis.html#keywords)
  * &shy;<!-- .element: class="fragment" -->Some words have a special meaning
* &shy;<!-- .element: class="fragment" -->Variables
  * &shy;<!-- .element: class="fragment" -->Words representing data
* &shy;<!-- .element: class="fragment" -->Expressions
  * &shy;<!-- .element: class="fragment" -->Perform an operation and return a value
* &shy;<!-- .element: class="fragment" -->Functions
  * &shy;<!-- .element: class="fragment" -->Code blocks that take input (arguments) and return output (values)

&shy;<!-- .element: class="fragment" -->**In python everything is an object**

---

### Built-in Python Data types

<div class="fragment">

| Type | Name | Declaration |
|------|------|-------------|
| Text | `string` | `"some text"` or `'some text'` |
| number | `integer`, `float`, and `complex` | `3`; `3.0`; `1+2j` |
| sequence | `list`, and `tuple` | `[1, 2, 3]`; `(1, 2, 3)` |
| mapping | `dictionary` | `{"key1": "v1", "key2": 2}` |
| sets | `set`, and `frozenset` | `{"a", "b", "c"}`; `frozenset({"a", "b", "c"})` |
| boolean | `boolean` | `True`, `False` |
| None | `NoneType` | `None` |

</div>

* &shy;<!-- .element: class="fragment" -->Use the function `type()` to get the data type!

---

### Let's get to coding

<div class="fragment">

``` python
>>> print(1 + 1)  # This is a comment
2

>>> print("Bioinformatics FTW")
Bioinformatics FTW
```

</div>

* &shy;<!-- .element: class="fragment" -->The `>>>` represents the interpreter's prompt. I will not be using it again
* &shy;<!-- .element: class="fragment" -->`print()` writes whatever valid code is inside the `()` to STDOUT
* &shy;<!-- .element: class="fragment" -->The `#` denotes a comment: text after it is not interpreted by python
  * &shy;<!-- .element: class="fragment" -->Great for writing down notes
* &shy;<!-- .element: class="fragment" -->The `1 + 1` in the first line is an expression

---

### Operators

* &shy;<!-- .element: class="fragment" -->Arithmetic operators
  * &shy;<!-- .element: class="fragment" -->`+`, `-`, `/`, `*`, `%`, `**`, and  `//`
  * &shy;<!-- .element: class="fragment" -->Always return a `numerical` variable

<div class="fragment">

``` python
1 + 2  # Sum

4 * 4  # Multiplication

2**2  # Exponentiation

3 / 2  # Division

3 % 2  # Remainder

6 // 7  # Floor division
```

</div>

|||

### Operators

* &shy;<!-- .element: class="fragment" -->Comparison operators
  * &shy;<!-- .element: class="fragment" -->`==`, `!=`, `>`, `<`, `>=` and `<=`
  * &shy;<!-- .element: class="fragment" -->Always return a `bool` variable

<div class="fragment">

``` python
2 == 2  # Equal

2 != 2  # Not equal

2 > 2  # Greater than

2 < 2  # Lesser than

2 >= 2  # Greater or equal to

2 <= 2  # Lesser or equal to
```

</div>

|||

### Operators

* &shy;<!-- .element: class="fragment" -->Logical operators
  * &shy;<!-- .element: class="fragment" -->`and`, `or`, `not`

<div class="fragment">

``` python
2 < 2 and 2 < 4  # Both conditions

2 < 2 or 2 < 4  # Either condition

not (2 < 2 or 2 < 4)  # The opposite of (reverse the condition)
```

**There are more operators, but for now these will suffice**

</div>

---

### Python variables

* &shy;<!-- .element: class="fragment" --><font color="#3c90e0">**Variables are the basic building blocks of programming**</font>
* &shy;<!-- .element: class="fragment" -->In python, variables:
  * &shy;<!-- .element: class="fragment" -->Always start with a *letter* or `_`
  * &shy;<!-- .element: class="fragment" -->Are followed by a combination of zero or more *letters*, *numbers*, or *underscore*
  * &shy;<!-- .element: class="fragment" -->Are **declared** with the operator `=` between the variable name and its value
  * &shy;<!-- .element: class="fragment" -->Are case sensitive
  * &shy;<!-- .element: class="fragment" -->Can be deleted with `del`
* &shy;<!-- .element: class="fragment" -->Strings and number variables

<div class="fragment">

``` python
# Dynamic typing!
bacteria = "Escherichia coli"  # String
strain = 'K-12'  # Also string
genome_length = 4641684  # Integer
del strain  # Kill it with fire
```

</div>

|||

### Python variables

* &shy;<!-- .element: class="fragment" -->Sequence variables

<div class="fragment">

``` python
Ecoli_strains = ["K-12", "B", "UPEC"]  # List with 3 elements

Ecoli_genomes = (4641684, 5363695, 5267261)  # Tuple with 3 elements

Ecoli_name = "Escherichia coli"  # A string is also a sequence!

print(Ecoli_strains)
print(Ecoli_genomes)
print(Ecoli_name)

# We can call sequence elements by their index value!
print(Ecoli_strains[1])  # What? Where did the first element go?
```

</div>

|||

### Python variables

* &shy;<!-- .element: class="fragment" -->Mapping variables

<div class="fragment">

``` python
# Dictionaries map "Keys" to "Values"

Ecoli_dict = {"K-12": 4641684,  # Listing the dict this way makes it more readable
              "B": 5363695,
              "UPEC": 5267261}

print(Ecoli_dict)
print(Ecoli_dict["K-12"])  # Calling a dict key returns the respective value
```

</div>

|||

### Python variables

* &shy;<!-- .element: class="fragment" -->Set variables

<div class="fragment">

``` python
# Sets have no order, or repeated measures

Ecoli_strains_found = {"K-12", "B", "K-12", "UPEC", "B"}
print(Ecoli_strains_found)  # Repeated elements are gone!
```

</div>


---

### Numeric type in depth 

#### Integer

<div class="fragment">

``` python
human_genes = 57771
human_population = <INSERT VALUE>  #  7979736581 at the time of writing

human_genes_in_world = human_genes * human_population
print(human_genes_in_world)
```

</div>

* &shy;<!-- .element: class="fragment" -->[World population estimate](https://www.worldometers.info/world-population/)
* &shy;<!-- .element: class="fragment" -->Python can deal with **very** large integers
* &shy;<!-- .element: class="fragment" -->The limit is the amount of memory
  * &shy;<!-- .element: class="fragment" -->[Not all languages can do this](https://arstechnica.com/information-technology/2015/05/boeing-787-dreamliners-contain-a-potentially-catastrophic-software-bug/)

|||

### Numeric type in depth 

#### Float

<div class="fragment">

``` python
2 * 2.0  # Float * int == float
2 + 2.0  # Float + int == float
4 / 2  # Int / int == float

3e3  # Scientific notation
3e-3

# The float precision is limited, however:
avg_exons = 8.9
percent_A = 0.24432
chrom_1_genes = 6417

try_1 = avg_exons * (percent_A * chrom_1_genes)
try_2 = (avg_exons * percent_A) * chrom_1_genes

print(try_1 - try_2)  # I know, right?
```

</div>

---

### That's it for today

---

### References

* [Stack overflow 2021 user survey](https://insights.stackoverflow.com/survey/2021)
* [Online python](https://www.online-python.com/)
* [Keywords](https://docs.python.org/3/reference/lexical_analysis.html#keywords)
* [W3 Schools Python datatypes](https://www.w3schools.com/python/python_datatypes.asp)
* [World population estimate](https://www.worldometers.info/world-population/)
* [Human genome assembly](https://www.ncbi.nlm.nih.gov/data-hub/genome/GCF_009914755.1/)
* [*E. coli* K-12 genome assembly](https://www.ncbi.nlm.nih.gov/data-hub/genome/GCF_001308125.1/)
* [*E. coli* B genome assembly](https://www.ncbi.nlm.nih.gov/data-hub/genome/GCF_907275105.1/)
* [*E. coli* UPEC genome assembly](https://www.ncbi.nlm.nih.gov/data-hub/genome/GCA_020301285.1/)
