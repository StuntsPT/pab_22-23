#!/usr/bin/env python3

import sys

# Coordinates

with open("ex07_coords.txt", "rt") as coords:
    for lines in coords:
        print(lines, end = "")


coord_storage = {}
with open("ex07_coords.txt", "rt") as coords:
    for lines in coords:
        lines = lines.rstrip().split()
        coord_storage[lines[0]] = tuple(lines[1:])

extra_coords = {"Tun": ("36.95", "8.85"), "Var": ("43.133333", "6.25")}
coord_storage = coord_storage | extra_coords

with open("ex07_new.txt", "wt") as new_coords:
    for sample, crds in coord_storage.items():
        line = f"{sample}\t{crds[0]}\t{crds[1]}\n"
        new_coords.write(line)


# Data filtering

DEGC = (72 - 32)/1.8
print(DEGC)

with open("ex07_envfile.txt", "rt") as envfile:
    header = envfile.readline()
    new_envfile = open("ex07_newenvfile.txt", "wt")
    new_envfile.write(header)
    for lines in envfile:
        lines = lines.split()
        degc = (float(lines[3]) - 32) / 1.8
        lines[3] = str(round(degc, 2))
        new_envfile.write("\t".join(lines) + "\n")
    new_envfile.close()

with open("ex07_newenvfile.txt", "rt") as envfile:
    greater_envfile = open("ex07_greaterenvfile.txt", "wt")
    for lines in envfile:
        lines = lines.split()
        lines.insert(1, coord_storage[lines[0]][0])
        lines.insert(2, coord_storage[lines[0]][1])
        greater_envfile.write("\t".join(lines) + "\n")
    greater_envfile.close()


# VCF parsing

with open("ex07_genotypes.vcf", "rt") as qsuber_vcf:
    for lines in qsuber_vcf:
        if lines.startswith("##"):
            continue
        if lines.startswith("#"):
            total_alleles = len(lines.split()[9:]) * 2
        lines = lines.split()
        NAME = "_".join(lines[:2])
        GENOTYPES = " ".join(lines[9:])
        missingpercent = round(GENOTYPES.count(".") / total_alleles * 100, 2)
        msg = f"{NAME}:\t{missingpercent}%"
        print(msg)

with open("ex07_genotypes.vcf", "rt") as qsuber_vcf:
    sample_missing_data = {}
    num_snps = 0
    for lines in qsuber_vcf:
        if lines.startswith("##"):
            continue
        if lines.startswith("#"):
            sample_names = lines.split()[9:]
            for name in sample_names:
                sample_missing_data[name] = 0
        else:
            genotypes = lines.split()[9:]
            for i in range(len(sample_names)):
                sample_missing_data[sample_names[i]] += genotypes[i].count(".")
        num_snps += 1

print(num_snps)
for k, v in sample_missing_data.items():
    print(f"{k}: {v/num_snps}")
