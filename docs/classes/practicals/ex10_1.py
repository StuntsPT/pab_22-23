#!/usr/bin/env python3

import sys

# 1


def reverse_complementer(sequence):
    """
    Reverse-complements a DNA sequence.
    Takes a sequence as input.
    Returns the reverse-complement sequence (upprecase)
    """
    rseq = sequence[::-1].lower()
    rcseq = rseq.replace("a", "T").replace("t", "A").replace("c", "G").replace("g", "C")
    return rcseq


# print(reverse_complementer("TAAAG"))

def start_finder(sequence):
    """
    Verifies is a sequence contains a start codon in the first 5 bases
    Take a sequence as input
    Returns True or False depending on whether a start codon is found
    """
    if "ATG" in sequence[:6]:
        return True
    else:
        return False


# print(start_finder("TATGGCATGACT"))
# print(start_finder("TTTGGCATGACT"))

def fasta_parser(fasta):
    """
    Parses a fasta file and stores the information therein in a dictionary
    Takes a FASTA filename as input
    Returns a dictionary like this: {"Sequence_name": "sequence", ...}
    """
    fdict = {}
    with open(fasta, "r") as fasta_handle:
        for lines in fasta_handle:
            if lines.startswith(">"):
                name = lines[1:].strip()
                fdict[name] = ""
            else:
                fdict[name] += lines.strip()
    return fdict


if __name__ == "__main__":
    print(reverse_complementer("TAAAG"))
    print(start_finder("TATGGCATGACT"))
    print(start_finder("TTTGGCATGACT"))
    print(fasta_parser(sys.argv[1]))
