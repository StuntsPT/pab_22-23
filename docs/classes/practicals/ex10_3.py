#!/usr/bin/env python

import json
import argparse
from Bio import SeqIO
from Bio.Seq import Seq

# Parse arguments
parser = argparse.ArgumentParser()
parser.add_argument("-i", "--input", type=str,
                    help="Path to input file")
parser.add_argument("-o", "--output", type=str,
                    help="Path to output file")
args = parser.parse_args()

# Perform the requested task
seqrecs = SeqIO.parse(args.input, "fasta")

jlist = []
for record in seqrecs:
    seqobject = {}
    seqobject["seqname"] = str(record.id)
    seqobject["sequence"] = str(record.seq)
    seqobject["length"] = len(record.seq)
    seqobject["translation"] = str(Seq(record.seq).translate())
    jlist.append(seqobject)

json_obj = json.dumps(jlist)
with open(args.output, "w") as jfile:
    jfile.write(json_obj)
