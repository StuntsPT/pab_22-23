#!/usr/bin/env python3

import os
import sys
import ex10_1

if os.path.isfile(sys.argv[1]):
    seqdict = ex10_1.fasta_parser(sys.argv[1])
    for name, seq in seqdict.items():
        print(ex10_1.start_finder(seq))
        print(ex10_1.reverse_complementer(seq))
elif any(x in ("A", "T", "C", "G") for x in sys.argv[1]):
    print(ex10_1.start_finder(sys.argv[1]))
    print(ex10_1.reverse_complementer(sys.argv[1]))
else:
    print("Argument is neither a file nor a DNA sequence.")
    sys.exit()
