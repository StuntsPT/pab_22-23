### classes.cd[6] = "File I/O"

#### Programação Aplicada à Bioinformática 2022-2023

![Logo EST](presentation_assets/logo-ESTB.png)

Francisco Pina Martins

[@FPinaMartins](https://twitter.com/FPinaMartins)

---

### Summary

* &shy;<!-- .element: class="fragment" -->How to handle files in python
* &shy;<!-- .element: class="fragment" -->`filehandle`
* &shy;<!-- .element: class="fragment" -->The concept of "cursor"
* &shy;<!-- .element: class="fragment" -->Reading files
* &shy;<!-- .element: class="fragment" -->Writing files

---

### Handling files in python

* &shy;<!-- .element: class="fragment" -->Python handles files via a special object:
  * &shy;<!-- .element: class="fragment" -->`file object`
  * &shy;<!-- .element: class="fragment" -->Contains an API to read or write file contents
* &shy;<!-- .element: class="fragment" -->It is obtained via the `open()` function call
  * &shy;<!-- .element: class="fragment" -->The resulting object is called a "file handle"
* &shy;<!-- .element: class="fragment" -->Today we will be using file I/O on a GFF3 file. ([What's this?](https://www.ncbi.nlm.nih.gov/datasets/docs/v1/reference-docs/file-formats/about-ncbi-gff3/#column-specifications)).
  * &shy;<!-- .element: class="fragment" -->Grab your copy [here](cd06_assets/SARS-COV-2.gff3)
  * &shy;<!-- .element: class="fragment" -->Save it on a location to where you know the full path
  * &shy;<!-- .element: class="fragment" -->Open it using any text editor

&shy;<!-- .element: class="fragment" -->![Covid](cd06_assets/sars-cov-2.webp) 


---

### Using the `open()` function

* &shy;<!-- .element: class="fragment" -->`open()` takes at least one argument - the path to file you want to access
* &shy;<!-- .element: class="fragment" -->Another important argument is `mode=`, which determines what will be done with the file:
  * &shy;<!-- .element: class="fragment" -->`"r"` for reading (default)
  * &shy;<!-- .element: class="fragment" -->`"w"` for writing
  * &shy;<!-- .element: class="fragment" -->`"a"` for appending
  * &shy;<!-- .element: class="fragment" -->`"t"` for handling text files (default)
  * &shy;<!-- .element: class="fragment" -->`"b"` for handling binary files
  * &shy;<!-- .element: class="fragment" -->...
* &shy;<!-- .element: class="fragment" -->`"b"` and `"t"` can be combined with any of the previous modes

<div class="fragment">

``` python
gff = open("/home/francisco/cd06/SARS-COV-2.gff3", "r")  # Use 'r' beacause explicit is better than implicit
print(type(gff))
print(gff)
```

</div>

---

### Reading whole filehandles

* &shy;<!-- .element: class="fragment" -->There are multiple ways to read file contents in python

<div class="fragment">

``` python
gff = open("/home/francisco/cd06/SARS-COV-2.gff3", "r")
gff_data = gff.read()  # As a string
gff.close()  # We're done here
print(gff_data)
```

</div>
<div class="fragment">

``` python
gff = open("/home/francisco/cd06/SARS-COV-2.gff3", "r")
gff_data = gff.readlines()  # As a list (each line is a list element of the type str())
gff.close()
print(gff_data)
```

</div>

* &shy;<!-- .element: class="fragment" -->Both approaches read the file from **start to end**
* &shy;<!-- .element: class="fragment" -->Both approaches place its entire contents in memory
* &shy;<!-- .element: class="fragment" -->Both approaches **are poor practice** except for small files

---

### Reading partial filehandles

* &shy;<!-- .element: class="fragment" -->Files can also be read partially

<div class="fragment">

``` python
gff = open("/home/francisco/cd06/SARS-COV-2.gff3", "r")
gff_data = gff.readline()  # As a string
gff.close()
print(gff_data)
```

</div>

* &shy;<!-- .element: class="fragment" -->The `.readline()` method reads a single line. How far did the cursor move?
  * &shy;<!-- .element: class="fragment" -->`.tell()` will tell you

<div class="fragment">

``` python
gff = open("/home/francisco/cd06/SARS-COV-2.gff3", "r")
gff.tell()  # 0 bytes - start of file
gff_data = gff.readline()  # As a string
gff.tell()  # 38 bytes, 1 byte per character. Try reading a file with a "ç"
gff.close()
print(gff_data)
```

</div>

* &shy;<!-- .element: class="fragment" -->In binary files `.seek()` moves the cursor
  * &shy;<!-- .element: class="fragment" -->Does not work well in text files

---

### Iterating over files

* &shy;<!-- .element: class="fragment" -->It is considered bad practice to fill memory with unused data
* &shy;<!-- .element: class="fragment" -->Python can avoid this issue by reading a file as an iterator

<div class="fragment">

``` python
gff = open("/home/francisco/cd06/SARS-COV-2.gff3", "r")
for lines in gff:
    print(lines, end="")  # Setting end="" avoids printing 2 newlines (one from the file, and one from print())
gff.close()
```

</div>

* &shy;<!-- .element: class="fragment" -->This will read each line, place it in memory, and replace it in the next iteration

|||

### A useful iteration

* &shy;<!-- .element: class="fragment" -->Print only the "spike protein" information

<div class="fragment">

``` python
gff = open("/home/francisco/cd06/SARS-COV-2.gff3", "r")
for lines in gff:
    if "spike" in lines:
        print(lines, end="")
        break
gff.close()
```

</div>

---

### Writing to files

* &shy;<!-- .element: class="fragment" -->Python can, of course, also write files
* &shy;<!-- .element: class="fragment" -->`.write()`

<div class="fragment">

``` python
text_file = open("/home/francisco/cd06/my_text.txt", "w")  # 'w' erases file contents and starts writing
text_file.write("Some example text")
text_file.close()
```

</div>
<div class="fragment">

``` python
text_file = open("/home/francisco/cd06/my_text.txt", "a")  # 'a' strats writing **after** the file's current contents
text_file.write("More example text")
text_file.close()
```

</div>
<div class="fragment">

``` python
text_file = open("/home/francisco/cd06/my_text.txt", "w")  # Destroy everything
text_file.write("HULK SMASH!")
text_file.close()
```

</div>

---

### Another useful example

* &shy;<!-- .element: class="fragment" -->Time to combine the previous lessons
* &shy;<!-- .element: class="fragment" -->Read the GFF file, and create a new file containing only 'genes'

<div class="fragment">

``` python
gff = open("/home/francisco/cd06/SARS-COV-2.gff3", "r")
genes = []
for lines in gff:
    if lines.startswith("#"):  # Skip the file header
        continue
    if lines.split()[2] == "gene":
        genes.append(lines)
gff.close()

gene_gff = open("/home/francisco/cd06/SARS-COV-2-genes.gff3", "w")
for gene in genes:
    gene_gff.write(gene)
gene_gff.close()
```

</div>

|||

### Alternative way to deal with file handles

* &shy;<!-- .element: class="fragment" -->The `with` keyword can be used to handle files
  * &shy;<!-- .element: class="fragment" -->Requires more space, but automatically closes handled files

<div class="fragment">

``` python
with open("/home/francisco/cd06/SARS-COV-2.gff3", "r") as gff:
    genes = []
    for lines in gff:
        if lines.startswith("#"):  # What happens without this conditional?
            continue
        if lines.split()[2] == "gene":
            genes.append(lines)

with open("/home/francisco/cd06/SARS-COV-2-genes.gff3", "w") as gene_gff:
    for gene in genes:
        gene_gff.write(gene)
```

</div>

---

### References

* [Introducing Python (Chapter 8)](https://www.oreilly.com/library/view/introducing-python-2nd/9781492051374/) (Paywall)
* [Python official docs on `open()`](https://docs.python.org/3/library/functions.html#open)
* [Python cursor](https://pynative.com/python-file-seek/) 
