### classes.cd[7] = "Functions"

#### Programação Aplicada à Bioinformática 2022-2023

![Logo EST](presentation_assets/logo-ESTB.png)

Francisco Pina Martins

[@FPinaMartins](https://twitter.com/FPinaMartins)

---

### Summary

* &shy;<!-- .element: class="fragment" -->Python functions
* &shy;<!-- .element: class="fragment" -->Variable scope
* &shy;<!-- .element: class="fragment" -->Function arguments
* &shy;<!-- .element: class="fragment" -->Function returns

---

### Python functions

* &shy;<!-- .element: class="fragment" -->Functions are a code block that **only runs when called**
* &shy;<!-- .element: class="fragment" -->This makes them "semi-independent" from the rest of the code
* &shy;<!-- .element: class="fragment" -->Variables declared inside functions only exist inside the function
* &shy;<!-- .element: class="fragment" -->Called by their name and parenthesis afterwards
    * &shy;<!-- .element: class="fragment" -->`print()`, `open()`, `type()`
* &shy;<!-- .element: class="fragment" -->Declared in Python using the `def` keyword
* &shy;<!-- .element: class="fragment" -->Useful when we need to perform an operation multiple times (but not only then!)


<div class="fragment" style="float:left; width:25%"

`$$f(x) = {1 \over 2} x$$`

</div>

<div class="fragment" style="float:right; width:50%"

![x-y plot](cd07_assets/xover2.png) 

</div>

---

### Writing a function

* &shy;<!-- .element: class="fragment" -->Let's start with writing and calling a simple function
* &shy;<!-- .element: class="fragment" -->Use the `def` keyword to declare it
* &shy;<!-- .element: class="fragment" -->Document your functions with a "docstring"
* &shy;<!-- .element: class="fragment" -->Remember: a function only runs when *called*

<div class="fragment">

``` python
def scoper():
    """
    Declares a variable and prints it
    """
    my_local_var = "Local"
    print(my_local_var)
scoper()  # Call the function. What happens if this line is deleted?
```

</div>

---

### Variable scope

* &shy;<!-- .element: class="fragment" -->Python variables can be "global" or "local"
    * &shy;<!-- .element: class="fragment" -->Global: declared *outside* any function
    * &shy;<!-- .element: class="fragment" -->Local: declared *inside* a function

<div class="fragment">

``` python
my_var = "Global"

def scoper():
    """
    Illustrates local and global variable use
    """
    print("Function:", my_var)
    my_local_var = "Local"
    print("Function:", my_local_var)
scoper()
print("Main:", my_var)
#print("Main:", my_local_var)  # Uncomment for error
```

</div>

* &shy;<!-- .element: class="fragment" -->Functions can access both global and local variables
    * &shy;<!-- .element: class="fragment" -->Local variables only exist *inside* the function
    * &shy;<!-- .element: class="fragment" -->This can cause conflicts!

&shy;<!-- .element: class="fragment" -->What if they clash?

|||

### Variable scope

* &shy;<!-- .element: class="fragment" -->Accessing a global variable with the same name as a local variable inside a function will FAIL

<div class="fragment">

``` python
my_var = "Global"

def scoper():
    """
    Illustrates local and global variable use
    """
    #print("Function:", my_var)  # Uncomment for error
    my_var = "Local"
    print("Function:", my_var)
scoper()
print("Main:", my_var)
```

</div>

&shy;<!-- .element: class="fragment" -->But... does it have to be that way?

|||

### Variable scope

* &shy;<!-- .element: class="fragment" -->It is, nevertheless, possible for functions to interact with the "global" scope
    * &shy;<!-- .element: class="fragment" -->Using the `global` keyword

<div class="fragment">

``` python
my_var = "Global"

def scoper():
    """
    Illustrates local and global variable use
    """
    global my_var
    print("Function:", my_var)  # This will work now, unlike previously
    my_var = "Local"
    print("Function:", my_var)
print("Main:", my_var)
scoper()
print("Main:", my_var)
```

</div>

&shy;<!-- .element: class="fragment" -->The fact this *can* be done does not mean it *should* be done

---

### Function input and output

* &shy;<!-- .element: class="fragment" -->Functions frequently take data input
    * &shy;<!-- .element: class="fragment" -->Called *arguments* (outside), or *parameters* (inside)
    * &shy;<!-- .element: class="fragment" -->Passed to the function inside the parenthesis
* &shy;<!-- .element: class="fragment" -->Functions frequently return some data
    * &shy;<!-- .element: class="fragment" -->Called *return value*

<div class="fragment">

``` python
def is_dna(seq):
    """
    Checks whether a string can be DNA.
    Takes a string as input (seq)
    Returns True if the sequence contains only DNA bases, otherwise False
    """
    bases = {"A", "C", "G", "T"}
    if set(seq.upper()).difference(bases) == set():
      dna = True
    else:
      dna = False
    return dna
print(is_dna("ATG"))
```

</div>

---

### Outside interaction 

* &shy;<!-- .element: class="fragment" -->It is good practice that functions interact only with local variables
* &shy;<!-- .element: class="fragment" -->It is, however, possible to make functions interact with global variables 

<div class="fragment">

``` python
my_numbers = []

def my_sum(x, y):
    """
    Sums two numbers, and appends the result to a **global** list
    Takes two number as input (x, and y)
    Has no return value
    """
    my_numbers.append(x + y)

print(my_numbers)
my_sum(1, 2)
print(my_numbers)
```

</div>

* &shy;<!-- .element: class="fragment" -->While this is somewhat manageable in small programs, it is simply **evil** to do it in larger programs
    * &shy;<!-- .element: class="fragment" -->Avoid at all cost (for your sanity's sake)

---

### More (on) arguments

* &shy;<!-- .element: class="fragment" -->Functions can take more than one argument
* &shy;<!-- .element: class="fragment" -->When a function is called, arguments can be provided in two ways:
    * &shy;<!-- .element: class="fragment" -->Positional - when placed in order
    * &shy;<!-- .element: class="fragment" -->Keyword - when assigned via their name

<div class="fragment">

``` python
def are_nucleotides(seq, bases):
    """
    Checks whether a string can be composed of nuclotides.
    Takes a string (seq) and a set (bases) as input
    Returns True if the sequence contains only DNA bases, otherwise False
    """
    if set(seq.upper()).difference(bases) == set():
      dna = True
    else:
      dna = False
    return dna

dna_bases = {"A", "C", "G", "T"}
print(are_nucleotides("ATG", dna_bases))  # Positional args
print(are_nucleotides(bases=dna_bases, seq="UCA"))  # Keyword args
```

</div>

---

### Default arguments

* &shy;<!-- .element: class="fragment" -->Functions can have arguments with pre-determined parameters
    * &shy;<!-- .element: class="fragment" -->"Default arguments" make arguments optional
    * &shy;<!-- .element: class="fragment" -->Arguments with default parameters **must** come after non-default arguments

<div class="fragment">

``` python
def are_nucleotides(seq, bases={"A", "C", "G", "T"}):
    """
    Checks whether a string can be composed of nuclotides.
    Takes a string (seq) and a set (bases) as input
    Returns True if the sequence contains only DNA bases, otherwise False
    """
    if set(seq.upper()).difference(bases) == set():
      dna = True
    else:
      dna = False
    return dna

rna_bases = {"A", "C", "G", "U"}  # Notice the "U"
print(are_nucleotides("ATG"))
print(are_nucleotides(bases=rna_bases, seq="UCA"))  # Notice the "U". This works because the default value was "overriden"
```

</div>

---

### A function in use

* &shy;<!-- .element: class="fragment" -->In this example we will use a function to make our code more explicit

<div class="fragment">

``` python
def are_nucleotides(seq, bases={"A", "C", "G", "T"}):
    """
    Checks whether a string can be composed of nuclotides.
    Takes a string (seq) and a set (bases) as input
    Returns True if the sequence contains only DNA bases, otherwise False
    """
    if set(seq.upper()).difference(bases) == set():
      dna = True
    else:
      dna = False
    return dna

my_sequences = {"My DNA sequence 01": "TCAGACGCATACGAGGCTGATATCGACTAGCTAGC",
                "My DNA sequence 02": "TCAGACGATCGATCGAGCATCGATCATAACCGGCA",
                "My protein sequence": "CJKAYCKASYCKAPQKCLQDUHCL",
                "My RNA sequence 01": "GCUAGCUACGACUAGCUGCUCGAUCGAUGCUACGGUA",
                "My DNA sequence 03": "ATCGATCGTAGGACGTAPACGATCGATCGATCGATC",
                }

for name, sequence in my_sequences.items():
    if "DNA" in name:
        my_bases = {"A", "C", "G", "T"}
    elif "RNA" in name:
        my_bases = {"A", "C", "G", "U"}
    else:
        print(f"Sequence '{name}' has no identified type.")
        continue
    if are_nucleotides(bases=my_bases, seq=sequence):
        msg = f"Sequence '{name}' contains only nucleotides"
    else:
        msg = f"Sequence '{name}' conatins something other than nucleotides"
    print(msg)
```

</div>

---

### References

* [Introducing Python (Chapter 4)](https://www.oreilly.com/library/view/introducing-python-2nd/9781492051374/) (Paywall)
* [Python official docs on function definitions](https://docs.python.org/3/reference/compound_stmts.html#function-definitions)
* [Pynative on functions](https://pynative.com/python-functions/)
